# This file is part of addts
# Copyright (C) 2018, 2019 Sergey Poznyakoff
# 
# Addts is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# Addts is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with addts.  If not, see <http://www.gnu.org/licenses/>.

PREFIX=/usr/local
PROJECT=addts
VERSION=1.0

# Installation directories:
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man
MAN1DIR=$(MANDIR)/man1

addts: addts.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -oaddts addts.c

clean:
	rm -f *.o addts

install: addts
	install addts $(DESTDIR)$(BINDIR)
	install addts.1 $(DESTDIR)$(MAN1DIR)

DISTDIR=$(PROJECT)-$(VERSION)
DISTFILES=addts.c addts.1 Makefile README NEWS 

distdir:
	test -d $(DISTDIR) || mkdir $(DISTDIR)
	cp $(DISTFILES) $(DISTDIR)

dist: distdir
	tar zcf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
	tar xfz $(DISTDIR).tar.gz
	@if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS); then \
	  echo "$(DISTDIR).tar.gz ready for distribution"; \
	  rm -rf $(DISTDIR); \
        else \
          exit 2; \
	fi

